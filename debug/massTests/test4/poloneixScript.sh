#This is the script that will repeatedly run the poloneix programs

#clear the chartData file
> ./dataFiles/chartData.dat
#clear the history
> ./dataFiles/history.dat
#reset the balances
echo "0.00000000" > ./dataFiles/balances.dat
echo "100.00000000" >> ./dataFiles/balances.dat

#set the counter to 0
counter=0;

while read temp
do
   #put each line from the master data into the chart data one by one
   echo $temp >> ./dataFiles/chartData.dat
   counter=$(($counter + 1))

   if [ $counter -gt $2 ]
   then
      #Call the python program to get the data from poloneix
      python ./pythonFiles/getDataPolo.py

      #Call the C++ program to process the data
      ../../../cppFiles/dataProcessing $1 $2 < ./dataFiles/ticker.dat > ./dataFiles/order.dat

      #get the order info and shove it into the history with date and time
      while read line
      do
         echo $(date) " : " $line >> ./dataFiles/history.dat
      done < ./dataFiles/order.dat

      #Call the next python program to buy or sell
      python ./pythonFiles/doTransaction.py

      #delete the first line of the chart file
      sed -i '1d' ./dataFiles/chartData.dat
   fi
done < ../../getMassData/chartMaster.dat

#print ending data
resultFileName="results$1$2.dat"

echo "TEST RESULTS" > ${resultFileName}
echo "Low: $1  High: $2" >> ${resultFileName}
echo >> ${resultFileName}
echo "ENDING BALANCES" >> ${resultFileName}
echo "BTC: " >> ${resultFileName}
sed -n '1p' < ./dataFiles/balances.dat >> ${resultFileName}
echo "XRP: " >> ${resultFileName}
sed -n '2p' < ./dataFiles/balances.dat >> ${resultFileName}

cat ${resultFileName}
