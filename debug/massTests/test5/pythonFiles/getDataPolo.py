import random
import json

#print "Getting Data......."

chartDataFile = "./dataFiles/chartData.dat"
tickerDataFile = "./dataFiles/ticker.dat"
balanceDataFile = "./dataFiles/balances.dat"
tickerData = []

#get the chart data
chart = open(chartDataFile, "r")

for line in chart:
    tickerData.append(line)

chart.close()

#get the balances we have right now
balance = open(balanceDataFile, "r")

BTCbalance = balance.readline()
XRPbalance = balance.readline()

balance.close()


#write all the data to the ticker.dat file
#open the file
ticker = open(tickerDataFile, "w")

#write the balances and the current conversion rate
ticker.write(BTCbalance)
ticker.write(XRPbalance)

#write the data from the ticker into the file
for i in tickerData:
    #ticker.write(format(i, '.8f') + "\n")
    ticker.write(i)

#Close the file
ticker.close()
