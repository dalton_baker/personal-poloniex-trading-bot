#initialize the number of points as 500
NUMPOINTS="$1"

#initialize the start time
CURRENTEPOCH=`date "+%s"`
CURRENTEPOCH=$(($CURRENTEPOCH - ((300 * $2)) ))

#get the start time
STARTEPOCH=$(($CURRENTEPOCH - ((300 * $NUMPOINTS)) ))

#create the json file from the output
echo -n "{\"chart\":" > chartMaster.dat
curl "https://poloniex.com/public?command=returnChartData&currencyPair=BTC_XRP&start="$STARTEPOCH"&end="$CURRENTEPOCH"&period=300" >> chartMaster.dat
echo -n "}" >> chartMaster.dat

#Convert the file into something readable with the python script
python convData.py

#print info to screen
echo "Start Date:"
date -d @$STARTEPOCH +"%m-%d-%Y %T"

echo "End Date:"
date -d @$CURRENTEPOCH +"%m-%d-%Y %T"
