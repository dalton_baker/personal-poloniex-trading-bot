import random
import json

print "Converting Data...."

tickerDataFile = "chartMaster.dat"
tickerData = []

#open the json file containing the ticker and extract the data
with open(tickerDataFile) as json_file:
    data = json.load(json_file)
    for p in data['chart']:
        tickerData.append(p["weightedAverage"])

#write all the data to the ticker.dat file
#open the file
ticker = open(tickerDataFile, "w")

#write the data from the ticker into the file
for i in tickerData:
    ticker.write(format(i, '.8f') + "\n")

#Close the file
ticker.close()
