#This is my secret api data, Don't let anyone get it!!!!
API_KEY="your_key_here"
API_SECRET="your_secret_here"
nonce=$((`date "+%s"` * 100000))


#this will generate the sign and then make it the correct format
API_SIGN=$(echo -n "command=returnBalances&nonce="$nonce | \
openssl sha512 -hmac $API_SECRET)
API_SIGN=${API_SIGN#"(stdin)= "}


#echo -n "{\"balances\":"
#Get the balances
curl -X POST \
   -d "command=returnBalances&nonce="$nonce \
   -H "Key: "$API_KEY \
   -H "Sign: "$API_SIGN \
   https://poloniex.com/tradingApi
#echo -n "}"
