import random
import json
import sys

#Tell the program what files to use
balanceDataFile = "./dataFiles/balances.dat"
currTickerFile = "./dataFiles/currTicker.dat"
chartDataFile = "./dataFiles/chart.dat"
tickerDataFile = "./dataFiles/ticker.dat"
tickerData = []

#Get your current balances
with open(balanceDataFile) as json_file:
    data = json.load(json_file)
    coin1bal = data[sys.argv[1]]
    coin2bal = data[sys.argv[2]]

#open the current ticker file as in json and get our data
with open(currTickerFile) as json_file:
    data = json.load(json_file)
    highBid = data[sys.argv[1] + "_" + sys.argv[2]]["highestBid"]
    lowAsk = data[sys.argv[1] + "_" + sys.argv[2]]["lowestAsk"]

#open the json file containing the ticker and extract the data
with open(chartDataFile) as json_file:
    data = json.load(json_file)
    for p in data['chart']:
        tickerData.append(p["close"])


#open the ticker file
ticker = open(tickerDataFile, "w")

#write the currency pair that we are dealing with
ticker.write(sys.argv[1] + "_" + sys.argv[2] + "\n")

#write the balances to the ticker file
ticker.write(coin1bal + "\n")
ticker.write(coin2bal + "\n")

#write the high ask and low bid to the ticker file
ticker.write(highBid + "\n")
ticker.write(lowAsk + "\n")

#write the data from the ticker into the file
for i in tickerData:
    ticker.write(format(i, '.8f') + "\n")

#Close the file
ticker.close()
