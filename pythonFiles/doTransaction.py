#Need to import library from python 3 to use print function I want
from __future__ import print_function
import sys

print ("Selling or Buying something")

#get the trade data
order = open("./dataFiles/order.dat", "r")

data = []
for line in order:
    data.append(line)

order.close()

#if there is no transactions, do nothing else
if len(data) < 3:
    print("   No Transactions")
    sys.exit()

#get the current balances
balances = open("./dataFiles/balances.dat", "r")

BTCbalance = balances.readline()
XRPbalance = balances.readline()

balances.close()

#if the order was for BTC process it
if data[0] == "BTC\n":
    if float(data[2]) > XRPbalance:
        print("   Not enough XRP!!!")
        sys.exit()
    else:
        newBTCbalance = float(BTCbalance) + float(data[1])
        newXRPbalance = float(XRPbalance) - float(data[2])

#if the order was for XRP process it
elif data[0] == "XRP\n":
    if float(data[2]) > BTCbalance:
        print("   Not enough BTC!!!")
        sys.exit()
    else:
        newXRPbalance = float(XRPbalance) + float(data[1])
        newBTCbalance = float(BTCbalance) - float(data[2])

print("  New BTC: " + str(newBTCbalance))
print("  New XRP: " + str(newXRPbalance))


#Write the new balances to the file
balances = open("./dataFiles/balances.dat", "w")

balances.write(format(newBTCbalance, '.8f') + "\n")
balances.write(format(newXRPbalance, '.8f') + "\n")

balances.close()
