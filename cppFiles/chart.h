/**************************************************************************//**
 * @file
 * @brief This is the header file for the ChartData class
 *****************************************************************************/
#ifndef __CHART__H__
#define __CHART__H__

#include <iostream>
#include <deque>

using std::deque;
using std::istream;
using std::cout;
using std::endl;

/*!
* @brief This object type hold chart data, balances, low ask, and high
* high bid values for a pair of currencies or any other type of chart.
* You could use this to track the value of any one thing to another thing.
* So you can use it for stock market data, currency exchange values, comodoty
* values, or, as is my case, values of crypto currencies against eachother.
*/
class ChartData
{
protected:
   /*! This is the name of the currency pair we are dealing with */
   char pair[20];
   /*! This is the current balance we have for the first currency */
   double curr1bal;
   /*! This is the current ballance for the second type of currency */
   double curr2bal;
   /*! This is the current high bid for currency 2 in terms of currency 1 */
   double highBid;
   /*! This is the low ask for currency 2 in terms of currency 1 */
   double lowAsk;
   /*! This is set of chart data, The front hold the most recent value */
   deque<double> chart;

public:
   ChartData(istream &fin);
   double SMA(int period);
   double EMA(int period);

   double convBal1to2();
   double convBal2to1();

   char* getPair();
   double getBal1();
   double getBal2();
   double getBid();
   double getAsk();
   deque<double> getChart();
   int getChartSize();

   void printChart(int numOfPoints);
};

#endif
