/**************************************************************************//**
 * @file
 * @brief Start of the program, only contains main.
 ****************************************************************************/

/**************************************************************************//**
 * @file
 *
 * @mainpage Data Processing For A Crypto Currency Trading Bot
 *
 * @authors Dalton Baker
 *
 * @date 8 / 31 / 2019
 *
 * @par Location:
 *         Rapid City, SD
 *
 * @section program_section Program Information
 *
 * @details This program will take in chart data, currency balances, high bid,
 * and low ask values and then decide if we should buy, sell, or hold our
 * our current position. I made it so that it can be changed fairly easily.
 *
 *    Most of the time you want to compair a very short moving average (small)
 * to a longer moving average (large) to guage if a stock or currency price is
 * making a sudden trend upward or downward compared to how it had moved in the
 * past. This can be acomplished with the two if statements in the middle of the
 * main function. The first one is determining if a sell should occur, and the
 * second is testing for a buy.
 *
 *    This program will mainly be used with the bash script I made it to work
 * with. However, the ChartData class I made for it could easily be used for a
 * completely different aplication. I only used c++ for this particular part of
 * the program because I think it's easier to use to process data. Also I'm not
 * fluent enough in bash or python to use them for this.
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions:
 *      make
 *
 * @par Usage:
 * No special usage requirements.
   @verbatim
   user@linux:~/program_dirrectory $ ./dataProcessing
   user@linux:~$ /path_to_dir/dataProcessing
   @endverbatim
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 *
 * @bug Nothing known currently
 *
 * @todo Everything is finished, program should be fully functional
 *
 * @par Modifications and Development Timeline:
 * Unfortunately I did not keep a timeline for this project.
 *
 ****************************************************************************/

#include <iostream>
#include <cmath>
#include <deque>
#include <stdlib.h>  // for strtol
#include "chart.h"

/*! This is only for debugging purposes */
#define DEBUG false

using namespace std;

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the main function, the program starts here. You can provide the small
 * and large values in the form of arguments 1 and 2 of the main program. If you
 * don't pass in arguments, small and large will be 50 and 200.
 *
 * @param[in] argc - The number of arguments provided to the program
 *
 * @param[in] argv - an array containing the arguments provided
 *
 * @returns 0 The program ran with no errors.
 *
 * @returns 1 Something went wrong in the program.
 *
 *****************************************************************************/
int main(int argc, char** argv)
{
   ChartData theChart(cin);

   int small = 50;
   int large = 200;

   if(argc > 1)
   {
      small = strtol(argv[1], NULL, 10);
      large = strtol(argv[2], NULL, 10);
   }

   //This is will print various data to the screen if you are debugging
   if(DEBUG)
   {
      cout << "Balances for " << theChart.getPair() << ": "
           << theChart.getBal1() << " ; " << theChart.getBal2()
           << "   TICKS: " << theChart.getChartSize() << endl << endl;

      cout << small << "SMA: " << theChart.SMA(small) << "   "
           << large << "SMA: " << theChart.SMA(large) << endl << endl;

      cout << small << "EMA: " << theChart.EMA(small) << "   "
           << large << "EMA: " << theChart.EMA(large) << endl << endl;

      cout << "High Bid : " << theChart.getBid() << "   Low Ask: "
           << theChart.getAsk() << endl << endl;

      theChart.printChart(theChart.getChartSize());

      return 0;
   }


   //if the small MA is less than the large MA then buy BTC
   if(theChart.SMA(small) < theChart.SMA(large) && theChart.getBal2() >= 1.0)
   {
      cout << theChart.getPair() << " sell " << theChart.getBid() << " ";
      cout << floor(theChart.getBal2()) << endl;
   }

   //if the small MA is greater than the large MA buy XRP
   if(theChart.SMA(small) > theChart.SMA(large) && theChart.convBal1to2() >= 1.0)
   {
      cout << theChart.getPair() << " buy " << theChart.getAsk() << " ";
      cout << floor(theChart.convBal1to2()) << endl;
   }


   return 0;
}
